package cz.swa.myhouse.contacts;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.LocalHostUriTemplateHandler;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class TestConfig
{
    @Bean
    @Primary
    public TestRestTemplate testRestTemplate(ApplicationContext context, RestTemplateBuilder templateBuilder)
    {
        final TestRestTemplate template = new TestRestTemplate(templateBuilder);
        template.setUriTemplateHandler(new LocalHostUriTemplateHandler(context.getEnvironment(), "http"));
        return template;
    }
}
