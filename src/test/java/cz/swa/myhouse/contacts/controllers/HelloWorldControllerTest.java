package cz.swa.myhouse.contacts.controllers;

import cz.swa.myhouse.contacts.ContactsProviderApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest(classes = ContactsProviderApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class HelloWorldControllerTest
{
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    public void getHelloWorld()
    {
        ResponseEntity<String> response  = restTemplate.getForEntity("/api/hello-world", String.class);

        // Assert.that(response.getBody().equals("Hello world!"));

        Assert.assertEquals("Hello world!", response.getBody());
    }
}
