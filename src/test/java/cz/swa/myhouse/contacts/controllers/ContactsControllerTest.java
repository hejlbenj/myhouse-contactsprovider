package cz.swa.myhouse.contacts.controllers;

import cz.swa.myhouse.contacts.ContactsProviderApplication;
import lombok.Data;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

// import javax.persistence.ElementCollection;
// import javax.persistence.GeneratedValue;
// import javax.persistence.GenerationType;
// import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = ContactsProviderApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations = "classpath:/application-test.properties")
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class ContactsControllerTest
{
    @Data
    private static class TestContact
    {
        private String id;
        private String name;
        private String phoneNumber;

        // @ElementCollection
        private List<String> tags = new ArrayList<>();

        // @ElementCollection
        private List<String> notes = new ArrayList<>();

        public boolean hasTag(String tag)
        {
            return tags.contains(tag);
        }
    }

    @Autowired
    TestRestTemplate restTemplate;

    @Before
    public void setup()
    {
        restTemplate.delete("/api/contacts/");
    }

    @Test
    public void test_insertContact_returns_inserted_contact_with_specified_id()
    {
        TestContact contact = new TestContact();
        contact.setId(null);
        contact.setName("John Smith");
        contact.setPhoneNumber("123456789");

        HttpEntity<TestContact> contactEntity = new HttpEntity<>(contact);
        ResponseEntity<TestContact> response  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        TestContact responseContact = response.getBody();

        Assert.assertEquals(responseContact.getName(), contact.getName());
        Assert.assertEquals(responseContact.getPhoneNumber(), contact.getPhoneNumber());
        Assert.assertNotEquals(responseContact.getId(), contact.getId());
    }

    @Test
    public void test_return_value_of_insertContact_is_same_as_from_getContact()
    {
        TestContact contact = new TestContact();
        contact.setId(null);
        contact.setName("John Smith");
        contact.setPhoneNumber("123456789");

        HttpEntity<TestContact> putRequest = new HttpEntity<>(contact);
        ResponseEntity<TestContact> putResponse  = restTemplate.exchange("/api/contacts/", HttpMethod.PUT, putRequest, TestContact.class);

        TestContact insertContact = putResponse.getBody();

        ResponseEntity<TestContact> getResponse  = restTemplate.exchange("/api/contacts/" + insertContact.getId(), HttpMethod.GET, null, TestContact.class);

        TestContact getContact = getResponse.getBody();

        Assert.assertEquals(insertContact, getContact);
    }

    @Test
    public void test_list_all_contacts_returns_empty_when_db_empty()
    {
        ResponseEntity<TestContact[]> getResponseEntity  = restTemplate.exchange("/api/contacts", HttpMethod.GET, null, TestContact[].class);

        TestContact[] contacts = getResponseEntity.getBody();

        Assert.assertEquals(0, contacts.length);
    }

    @Test
    public void test_list_all_contacts_with_query()
    {
        List<String> electricianTag = new ArrayList<>();
        electricianTag.add("electrician");
        List<String> plumberTags = new ArrayList<>();
        electricianTag.add("plumber");


        TestContact electrician = new TestContact();
        electrician.setName("John Adams");
        electrician.setPhoneNumber("123456789");
        electrician.setTags(electricianTag);

        TestContact plumber = new TestContact();
        plumber.setName("Jane Adams");
        plumber.setPhoneNumber("123456789");
        plumber.setTags(plumberTags);

        HttpEntity<TestContact> contactEntity = new HttpEntity<>(electrician);
        ResponseEntity<TestContact> putResponse  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        contactEntity = new HttpEntity<>(plumber);
        putResponse  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        Map<String, String> query = new HashMap<>();
        query.put("name", "John Adams");

        HttpEntity<String> dummyEntity = new HttpEntity<>("");
        ResponseEntity<TestContact[]> getResponseEntity  = restTemplate.exchange("/api/contacts?name={name}", HttpMethod.GET, dummyEntity, TestContact[].class, query);

        TestContact[] contacts = getResponseEntity.getBody();

        Assert.assertEquals(1, contacts.length);
    }

    @Test
    public void test_list_all_contacts_with_tag()
    {
        List<String> electricianTag = new ArrayList<>();
        electricianTag.add("electrician");
        List<String> plumberTags = new ArrayList<>();
        electricianTag.add("plumber");


        TestContact electrician = new TestContact();
        electrician.setName("John Adams");
        electrician.setPhoneNumber("123456789");
        electrician.setTags(electricianTag);

        TestContact plumber = new TestContact();
        plumber.setName("Jane Adams");
        plumber.setPhoneNumber("123456789");
        plumber.setTags(plumberTags);

        HttpEntity<TestContact> contactEntity = new HttpEntity<>(electrician);
        ResponseEntity<TestContact> putResponse  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        contactEntity = new HttpEntity<>(plumber);
        putResponse  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        Map<String, String> query = new HashMap<>();
        query.put("name", "John Adams");

        HttpEntity<String> dummyEntity = new HttpEntity<>("");
        ResponseEntity<TestContact[]> getResponseEntity  = restTemplate.exchange("/api/contacts/with-tag/electrician", HttpMethod.GET, dummyEntity, TestContact[].class, query);

        TestContact[] contacts = getResponseEntity.getBody();

        Assert.assertEquals(1, contacts.length);
    }

    @Test
    public void test_addNotes_returns_the_contact_with_provided_notes()
    {
        List<String> electricianTag = new ArrayList<>();
        electricianTag.add("electrician");

        TestContact electrician = new TestContact();
        electrician.setName("John Adams");
        electrician.setPhoneNumber("123456789");
        electrician.setTags(electricianTag);

        HttpEntity<TestContact> contactEntity = new HttpEntity<>(electrician);
        ResponseEntity<TestContact> putResponse  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        contactEntity = new HttpEntity<>(electrician);
        putResponse  = restTemplate.exchange("/api/contacts", HttpMethod.PUT, contactEntity, TestContact.class);

        String notes = "cool guy";

        HttpEntity<String> dummyEntity = new HttpEntity<>(notes);
        ResponseEntity<TestContact> getResponseEntity  = restTemplate.exchange("/api/contacts/" + putResponse.getBody().getId() + "/notes", HttpMethod.PUT, dummyEntity, TestContact.class);

        TestContact contact = getResponseEntity.getBody();

        Assert.assertEquals(contact.getNotes().contains(notes), true);
    }
}
