package cz.swa.myhouse.contacts.controllers;

import cz.swa.myhouse.contacts.models.Contact;
import cz.swa.myhouse.contacts.services.AuthorizationService;
import cz.swa.myhouse.contacts.services.ContactsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/contacts")
@Slf4j
public class ContactsController
{
    @Autowired
    private ContactsService contactsService;

    @Autowired
    private AuthorizationService authorizationService;

    @Operation(summary = "List contacts which conforms to the query.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200", description = "Contacts found", content = {
                    @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Contact.class)))
            }),
            @ApiResponse(responseCode = "404", description = "No contacts found", content = @Content)
    })
    @GetMapping("")
    public ResponseEntity<List<Contact>> listContactsWithQuery(@RequestParam Map<String, String> query)
    {
        try
        {
            log.info("ContactsController: listContactsWithQuery");
            return new ResponseEntity<>(contactsService.listContactsWithQuery(query), HttpStatus.OK);
        }
        catch (Exception e)
        {
            log.error("ContactsController: Error during listContactsWithQuery: ", e);
            throw e;
        }
    }


    @Operation(summary = "List contacts which conforms to the query and has the specified tag.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200", description = "Contacts found", content = {
                    @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Contact.class)))
            }),
            @ApiResponse(responseCode = "404", description = "No contacts found", content = @Content)
    })
    @GetMapping("/with-tag/{tag}")
    public List<Contact> listContactsWithTagAndQuery(@PathVariable("tag") String tag, @RequestParam Map<String, String> query)
    {
        try
        {
            log.info("ContactsController: listContactsWithTagAndQuery");
            return contactsService.listContactsWithTag(tag);
        }
            catch (Exception e)
        {
            log.error("ContactsController: Error during listContactsWithTagAndQuery: ", e);
            throw e;
        }
    }


    @Operation(summary = "Get a contact by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the contact", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Contact.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Contact was not found.", content = @Content)
    })
    @GetMapping("/{id}")
    public Contact getContactWithId(@PathVariable("id") String id)
    {
        try
        {
            log.info("ContactsController: getContactWithId");
            return contactsService.getContactById(id);
        }
        catch (Exception e)
        {
            log.error("ContactsController: Error during getContactWithId: ", e);
            throw e;
        }
    }


    @Operation(summary = "Inserts a new contact with supplied values")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Contact inserted or updated", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Contact.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content), // weird, how to ensure?
            @ApiResponse(responseCode = "404", description = "Contact not found", content = @Content)    // weird, how to ensure?
    })
    @PutMapping("")
    public Contact insertContact(@RequestBody Contact contact)
    {
        try
        {
            log.info("ContactsController: insertContact");
            return contactsService.insertContact(contact);
        }
        catch (Exception e)
        {
            log.error("ContactsController: Error during insertContact: ", e);
            throw e;
        }
    }


    @Operation(summary = "Deletes a contact with supplied id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Contact deleted", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Contact.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content), // weird, how to ensure?
            @ApiResponse(responseCode = "404", description = "Contact not found", content = @Content)    // weird, how to ensure?
    })
    @DeleteMapping("/{id}")
    public Contact deleteContact(@PathVariable("id") String id)
    {
        try
        {
            log.info("ContactsController: deleteContact");
            return contactsService.deleteContact(id);
        }
        catch (Exception e)
        {
            log.error("ContactsController: Error during deleteContact: ", e);
            throw e;
        }
    }


    @Operation(summary = "Deletes contacts with supplied query")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Contacts deleted", content = {
                    @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Contact.class)))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content), // weird, how to ensure?
            @ApiResponse(responseCode = "404", description = "Contact not found", content = @Content)    // weird, how to ensure?
    })
    @DeleteMapping("")
    public List<Contact> deleteContactsWithQuery(@RequestParam Map<String, String> query)
    {
        try
        {
            log.info("ContactsController: deleteContactsWithQuery");
            return contactsService.deleteContacts(query);
        }
        catch (Exception e)
        {
            log.error("ContactsController: Error during deleteContactsWithQuery: ", e);
            throw e;
        }
    }

    @Operation(summary = "Adds notes to a contact with supplied id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Notes inserted", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Contact.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content), // weird, how to ensure?
            @ApiResponse(responseCode = "404", description = "Contact not found", content = @Content)    // weird, how to ensure?
    })
    @PutMapping("/{id}/notes")
    public Contact addNotes(@PathVariable("id") String id, @RequestBody String note)
    {
        try
        {
            log.info("ContactsController: addNotes");
            return contactsService.addNotes(id, note);
        }
        catch (Exception e)
        {
            log.error("ContactsController: Error during addNotes: ", e);
            throw e;
        }
    }
}
