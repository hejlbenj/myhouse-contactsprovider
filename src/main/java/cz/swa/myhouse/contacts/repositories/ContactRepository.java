package cz.swa.myhouse.contacts.repositories;

import cz.swa.myhouse.contacts.models.Contact;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
// public interface ContactRepository extends JpaRepository<Contact, Long>
public interface ContactRepository extends MongoRepository<Contact, String>
{
    Contact findContactById(String contactId);

    void deleteAllById(String id);
}
