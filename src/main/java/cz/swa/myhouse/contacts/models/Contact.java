package cz.swa.myhouse.contacts.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


// import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
@Data
@ToString
@Document(collection = "contacts")
public class Contact
{

    @Transient
    public static final String SEQUENCE_NAME = "contacts_sequence";

    @Id
    private String id;

    @NotNull(message = "Contact name must not be null")
    @Size(min = 1, message = "Contact name must not be empty.")
    private String name;

    @NotNull(message = "Contact phone number must not be null")
    @Pattern(regexp = "[0-9]{9}")
    private String phoneNumber;

    // @ElementCollection
    private List<String> tags = new ArrayList<>();

    // @ElementCollection
    private List<String> notes = new ArrayList<>();

    public boolean hasTag(String tag)
    {
        return tags.contains(tag);
    }
}
