package cz.swa.myhouse.contacts.services;

import cz.swa.myhouse.contacts.models.Contact;
import cz.swa.myhouse.contacts.repositories.ContactRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ContactsService
{
    @Autowired
    private ContactRepository contactRepository;

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PHONE_NUMBER = "phoneNumber";
    private static final String TAG = "tag";

    private static boolean checkRegex(String pattern, String value)
    {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(value);
        return m.find();
    }

    private static boolean checkRegex(String pattern, List<String> values)
    {
        Pattern p = Pattern.compile(pattern);

        for (String v : values)
        {
            if (p.matcher(v).find()) return true;
        }

        return false;
    }

    /*
     * returns true if the contact conforms the query, false otherwise
     */
    private static boolean contactSelector(Contact contact, Map<String, String> query)
    {
        // check id
        if (query.containsKey(ID) && !query.get(ID).equals(contact.getId()))
        {
            return false;
        }

        // check name
        if (query.containsKey(NAME) && !checkRegex(query.get(NAME), contact.getName()))
        {
            return false;
        }

        // check phone number
        if (query.containsKey(PHONE_NUMBER) && !checkRegex(query.get(PHONE_NUMBER), contact.getPhoneNumber()))
        {
            return false;
        }

        // check tag
        if (query.containsKey(TAG) && !checkRegex(query.get(TAG), contact.getTags()))
        {
            return false;
        }

        // check other properties ??

        return true;
    }

    private static String writeQuery(Map<String, String> query)
    {
        StringBuilder builder = new StringBuilder();

        for(Map.Entry<String, String> e : query.entrySet())
        {
            builder.append(e.getKey()).append("=").append(e.getValue()).append(" &");
        }

        return builder.toString();
    }



    public List<Contact> listContacts()
    {
        log.info("ContactsService: listContacts");

        return contactRepository.findAll();
    }

    public List<Contact> listContactsWithQuery(Map<String, String> query)
    {
        log.info("ContactsService: listContactsWithQuery, query: " + writeQuery(query));

        return contactRepository
                .findAll()
                .stream()
                .filter(c -> contactSelector(c, query))
                .collect(Collectors.toList());
    }

    public List<Contact> listContactsWithTag(String tag)
    {
        log.info("ContactsService: listContactsWithTag, tag: " + tag);

        return contactRepository
                .findAll()
                .stream()
                .filter(c -> c.getTags() != null && c.getTags().contains(tag))
                .collect(Collectors.toList());
    }

    public List<Contact> listContactsWithTagAndQuery(String tag, Map<String, String> query)
    {
        log.info("ContactsService: listContactsWithTagAndQuery, tag: " + tag + " query: " + writeQuery(query));

        return contactRepository
                .findAll()
                .stream()
                .filter(c -> c.getTags() != null && c.getTags().contains(tag) && contactSelector(c, query))
                .collect(Collectors.toList());
    }

    public Contact getContactById(String id)
    {
        log.info("ContactsService : getContactById, id: " + id);

        return contactRepository.findContactById(id);
    }

    public Contact insertContact(Contact contact)
    {
        log.info("ContactsService : insertContact, contact: " + contact.toString());

        return contactRepository.save(contact);
    }

    public Contact deleteContact(String id)
    {
        log.info("ContactsService : deleteContact, id: " + id);

        Contact contact = contactRepository.findContactById(id);
        // contactRepository.deleteById(id);
        contactRepository.deleteAllById(id);
        // contactRepository.delete(contact);

        return contact;
    }

    public Contact addNotes(String id, String note)
    {
        log.info("ContactsService : addNotes, id: " + id + " note: " + note);

        Contact contact = contactRepository.findContactById(id);

        List<String> notes = contact.getNotes();
        if (notes == null)
        {
            notes = new ArrayList<>();
        }
        notes.add(note);

        contact.setNotes(notes);

        contactRepository.save(contact);

        return contact;
    }

    public List<Contact> deleteContacts(Map<String, String> query) 
    {
        log.info("ContactsService: listContactsWithQuery, query: " + writeQuery(query));

        List<Contact> contacts = contactRepository
                .findAll()
                .stream()
                .filter(c -> contactSelector(c, query))
                .collect(Collectors.toList());

        for (Contact c : contacts)
        {
            deleteContact(c.getId());
            c.setId(null);
        }
        return contacts;
    }
}
