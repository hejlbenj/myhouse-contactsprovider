package cz.swa.myhouse.contacts.services;

import cz.swa.myhouse.contacts.configuration.AuthorizationServiceConfiguration;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class AuthorizationService
{
    private static class Authorization
    {
        private String id;
        private String userId;
        private String serviceId;
        private String authorizationLevel;
    }

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AuthorizationServiceConfiguration configuration;

    public AuthorizationResult TryAuthorize(String userName, String password)
    {
        ResponseEntity<Authorization> response = restTemplate.getForEntity(configuration.getProtocol() + "//" + configuration.getHost() + ":" + configuration.getPort() + "/authorizations/" + userName, Authorization.class);

        HttpStatus status = response.getStatusCode();

        return AuthorizationResult.VALID;
    }
}
