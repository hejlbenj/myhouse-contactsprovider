package cz.swa.myhouse.contacts.services;

public enum AuthorizationResult {
    VALID,
    INVALID,
    SERVICE_DOWN
}
