package cz.swa.myhouse.contacts.configuration;

import lombok.*;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Validated
@ConstructorBinding
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EndpointConfiguration
{
    @NotBlank
    @Getter
    @Setter
    private String host;

    @Min(1025)
    @Max(65536)
    @Getter
    @Setter
    private int port;
}
