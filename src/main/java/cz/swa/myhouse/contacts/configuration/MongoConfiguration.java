package cz.swa.myhouse.contacts.configuration;


import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@ConfigurationProperties(prefix = "spring.data.mongodb")
@Configuration
@EnableMongoRepositories(basePackages = "cz.swa.myhouse.contacts")
public class MongoConfiguration extends EndpointConfiguration
{
    @Configuration
    public static class MongoClientConfiguration extends AbstractMongoClientConfiguration
    {
        @Autowired
        private MongoConfiguration mongoConfiguration;

        @Override
        protected String getDatabaseName()
        {
            return "myhouse-contacts";
        }

        @Override
        public MongoClient mongoClient()
        {
            ConnectionString connectionString = new ConnectionString("mongodb://" + mongoConfiguration.getHost() + ":" + mongoConfiguration.getPort() + "/" + getDatabaseName());

            MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                    .applyConnectionString(connectionString)
                    .build();

            return MongoClients.create(mongoClientSettings);
        }
    }
}
