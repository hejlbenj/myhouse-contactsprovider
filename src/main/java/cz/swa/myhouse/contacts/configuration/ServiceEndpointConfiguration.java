package cz.swa.myhouse.contacts.configuration;

import lombok.*;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Validated
@ConstructorBinding
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ServiceEndpointConfiguration extends EndpointConfiguration
{

    @NotBlank
    @Getter
    @Setter
    private String id;

    @Pattern(regexp = "https?")
    @NotBlank
    @Getter
    @Setter
    private String protocol;
}
