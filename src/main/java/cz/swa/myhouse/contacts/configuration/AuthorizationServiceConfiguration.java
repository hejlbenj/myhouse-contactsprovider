package cz.swa.myhouse.contacts.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@ConfigurationProperties(prefix = "my-house.authorization")
@Configuration
public class AuthorizationServiceConfiguration extends ServiceEndpointConfiguration
{
}
