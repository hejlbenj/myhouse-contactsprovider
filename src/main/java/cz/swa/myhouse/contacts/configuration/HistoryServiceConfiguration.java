package cz.swa.myhouse.contacts.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "my-house.history")
@Configuration
public class HistoryServiceConfiguration extends ServiceEndpointConfiguration
{

}
