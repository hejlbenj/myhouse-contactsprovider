package cz.swa.myhouse.contacts.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConfigurationPrinter implements ApplicationRunner
{
    @Autowired
    private AuthorizationServiceConfiguration authorizationConfiguration;

    @Autowired
    private DocumentsServiceConfiguration documentsConfiguration;

    @Autowired
    private HistoryServiceConfiguration historyConfiguration;

    @Autowired
    private RemindersServiceConfiguration remindersConfiguration;

    @Autowired
    private ContactsServiceConfiguration serverConfiguration;


    @Override
    public void run(ApplicationArguments args) throws Exception
    {
        printEndpoint(serverConfiguration);
        printEndpoint(authorizationConfiguration);
        printEndpoint(documentsConfiguration);
        printEndpoint(historyConfiguration);
        printEndpoint(remindersConfiguration);
    }

    void printEndpoint(ServiceEndpointConfiguration endpoint)
    {
        log.info("Endpoint for {}: http://{}:{}", endpoint.getId(), endpoint.getHost(), endpoint.getPort());
    }
}
