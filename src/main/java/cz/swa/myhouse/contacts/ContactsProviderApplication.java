package cz.swa.myhouse.contacts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ContactsProviderApplication
{
	@Bean
	@Primary
	public RestTemplate restTemplate(ApplicationContext context, RestTemplateBuilder templateBuilder)
	{
		return templateBuilder.build();
	}

	public static void main(String[] args)
	{
		SpringApplication.run(ContactsProviderApplication.class, args);
	}

}
