FROM maven:latest as builder

ADD . /usr/src

WORKDIR /usr/src
RUN mvn package


FROM openjdk:11-jre-slim-buster as app

COPY --from=builder /usr/src/target/myhouse-contactsprovider-0.0.1-SNAPSHOT.jar /usr/src/app/

WORKDIR /usr/src/app

CMD java -jar myhouse-contactsprovider-0.0.1-SNAPSHOT.jar
